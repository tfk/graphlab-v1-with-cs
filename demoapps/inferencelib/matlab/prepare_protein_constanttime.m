% addpath('/media/d/Documents/Canberra/project/fshsic/papers/meanField/codes'); 
% addpath('/media/d/Documents/Canberra/project/fshsic/papers/meanField/codes/uLSIF'); 
% addpath('/media/d/Documents/Canberra/project/fshsic/papers/meanField/codes/itpp-4.0.7/extras/');
% 
% prefix = '../Join_PDB30_Matlab/';

addpath('uLSIF'); 
prefix = 'Join_PDB30_Matlab/';

if (1)
    fprintf(1, '--counting the number of proteins ...\n');    
    fid = fopen([prefix, 'filterlist.txt']);
    totalno = 0; 
    while 1
        tline = fgetl(fid); 
        if ~ischar(tline), break, end

        tmp = load([prefix, tline, '.txt']);
        
        m = size(tmp, 1);       
        if (m < 100)
            continue; 
        end        
        
        idx = find(tmp(:,2) < -500 | tmp(:,3) < -500);                
        if (length(idx) > 1)
            continue;
        end
        
        totalno = totalno + 1;         
    end
    fclose(fid);     
    fprintf(1, '--%d in total\n', totalno);        
       
    angle_tCell = cell(1, totalno); 
    atype_tCell = cell(1, totalno);     
    feat_tCell = cell(1, totalno); 
    
    angle_tpCell = cell(1, totalno); 
    atype_tpCell = cell(1, totalno);         

    angleCell = cell(1, totalno); 
    featCell = cell(1, totalno); 
    atypeCell = cell(1, totalno);
    lenCell = zeros(1, totalno); 
   
    fid = fopen([prefix, 'filterlist.txt']);    
    iter = 0;    
    while 1
        tline = fgetl(fid); 
        if ~ischar(tline), break, end

        tmp = load([prefix, tline, '.txt']);
        
        m = size(tmp, 1);       
        if (m < 100)
            continue; 
        end        
        
        idx = find(tmp(:,2) < -500 | tmp(:,3) < -500);                
        if (length(idx) > 1)
            continue;
        end
        if isempty(idx)
            idx = 0;
        end

        iter = iter + 1; 
        fprintf(1, '--processing file %d: %s\n', iter, tline);         
        
        % data at position t; 
        angle_tCell{iter} = tmp((idx+1):(end-1), 2:3)';
        atype_tCell{iter} = tmp((idx+1):(end-1), 1)';        
        feat_tCell{iter} = [tmp((idx+1):(end-1), 4:23)'; ones(1,length(atype_tCell{iter}))];
        
        % data at position t+1; 
        angle_tpCell{iter} = tmp((idx+2):end, 2:3)';        
        atype_tpCell{iter} = tmp((idx+2):end, 1)';
                
        % all data; 
        angleCell{iter} = tmp((idx+1):end, 2:3)';
        atypeCell{iter} = tmp((idx+1):end, 1)';        
        featCell{iter} = [tmp((idx+1):end, 4:23)'; ones(1,length(atypeCell{iter}))]; 
        lenCell(iter) = length(atypeCell{iter}); 
    end
    fclose(fid); 
    
    if (iter ~= totalno)
        fprintf(1, '--number of protein does not match.\n'); 
        return;
    end; 
    
%     save([prefix, 'traindata.mat'], 'angle_t', 'angle_tp', 'fangle_t', 'feat_t');
    
end

errmat = zeros(1,sum(lenCell));
curidx = 0; 
foldno = 10;
rand('twister', foldno);
rand_idx = randperm(totalno); 

for resono = [1,3,6]
for k = 1:foldno
    fprintf(1, '--processing fold %d of %d\n', k, foldno); 
    
    test_idx = rand_idx(k:foldno:totalno); 
    train_idx = setdiff(rand_idx, test_idx); 
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % training; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % get training data; 
    angle_t = cell2mat(angle_tCell(train_idx)); 
    feat_t = cell2mat(feat_tCell(train_idx));
    atype_t = cell2mat(atype_tCell(train_idx)); 
    
    angle_tp = cell2mat(angle_tpCell(train_idx));     
    atype_tp = cell2mat(atype_tpCell(train_idx));
    
    % transform training angle to coordinate on sphere; 
    coord_t = angle2coord2(angle_t); 
    coord_tp = angle2coord2(angle_tp); 
    
    ano = length(unique(atype_t));
%     % standardize the features;
%     [tmpfeat_t, mf, sf] = standardize(feat_t(1:end-1,:)); 
%     feat_t = [tmpfeat_t; feat_t(end,:)]; 
    xdim = size(feat_t,1);     
    testlen = length(test_idx);
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % decompostion of kernel; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     fxno = 200; 
%     tmpidx = randperm(size(feat_t,2)); 
%     tmpno = min(3000, length(tmpidx)); 
%     dismatx = pdist(feat_t(:,tmpidx(1:tmpno))').^2; 
%     mdisx = median(dismatx); 
%     sx = 0.1/mdisx;    
%     
%     expno = 1; 
%     [fx_t, Ax, Ix] = incomplete_cholRBF(feat_t, sx, expno, fxno);
%     fxno = length(Ix);
%     xbasis = feat_t(:, Ix);
%     fxbasis = fx_t(:, Ix);    
%     feat_t = fx_t; 
%     xdim = size(feat_t,1); 
    
    fno = 100; 
    tmpidx = randperm(size(coord_t,2)); 
    tmpno = min(3000, length(tmpidx)); 
    dismat = pdist(coord_t(:,tmpidx(1:tmpno))').^2; 
    mdis = median(dismat); 
    s = 0.5/mdis; 
    
    expno = 1; 
    reso = 10.^(-resono); 
    [fy_t, A, I] = incomplete_cholRBF(coord_t, s, expno, fno, reso);
    fno = length(I);
    ybasis = coord_t(:, I);
    fybasis = fy_t(:, I);

    fy_tp = incomplete_cholRBF_test(coord_tp, ybasis, s, expno); 
%     ffy_t = incomplete_cholRBF_test(fcoord_t, ybasis, s, expno); 

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % decompostion of kernel on test points;  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ang1 = (-179:5:180);
    lang1 = length(ang1);
    ang2 = (1:5:180); 
    lang2 = length(ang2);
    ang = [kron(ang1, ones(1,lang2)); repmat(ang2, 1, lang1)]; 
    testy = angle2coord2(ang);
    testno = length(testy); 
    ftesty = incomplete_cholRBF_test(testy, ybasis, s, expno);
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % decomposition of kernel to power 2; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fno2 = 200; 
    expno2 = 2; 
    [fy2_t, A2, I2] = incomplete_cholRBF(coord_t, s, expno2, fno2, reso);
    fno2 = length(I2);
    ybasis2 = coord_t(:, I2);
    fybasis2 = fy2_t(:, I2);

    cfybasis = fy_t(:, I2); 
    cfybasis2 = fy2_t(:, I); 
    
    fy2_tp = incomplete_cholRBF_test(coord_tp, ybasis2, s, expno2); 
    
    itsave([prefix, 'KBP', int2str(resono), '_fold', int2str(k), '_common', '.it'], ...
        cfybasis, ftesty, ano, testy, testlen);     
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Compute operators; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    lamfy0 = 1e-1;        
    iCfyfy = cell(1, ano); 
    Cfyx = cell(1, ano);
    iCxx = cell(1, ano);
    fmu = cell(1, ano); 
    lamx = 1e-3;     

    for i = 1:ano
        idx = find(atype_t == i); 
        Cfyx{i} = fy_t(:,idx) * feat_t(:,idx)' ./ length(idx); 
        Cfyfy = fy_t(:,idx) * fy_t(:,idx)' ./ length(idx); 
        iCfyfy{i} = inv(Cfyfy + lamfy0 * eye(fno)); 
        
        iCxx{i} = inv(feat_t(:,idx) * feat_t(:,idx)' ./ length(idx) + lamx * eye(xdim));        
        fmu{i} = mean(fy_t(:,idx), 2);   
        
        Cfyxi = Cfyx{i}; 
        iCfyfyi = iCfyfy{i}; 
        iCxxi = iCxx{i}; 
        fmui = fmu{i}; 
        itsave([prefix, 'KBP', int2str(resono), '_fold', int2str(k), '_node', int2str(i), '.it'], ...
            Cfyxi, iCfyfyi, iCxxi, fmui);                       
        
    end        
    
    lamfy = 1e-1; 
    Uttp = cell(ano,ano);
    Uttp0 = cell(ano, ano);     
    Utpt = cell(ano,ano); 
    Utpt0 = cell(ano,ano);    
    
    for i = 1:ano
        for j = 1:ano
            idx = find(atype_t == i & atype_tp == j);            
            fprintf(1, '--amino acid type %d vs %d, data no %d\n', i, j, length(idx)); 
            % Operator for passing message backward;                        
            Cttp = fy_t(:,idx) * fy2_tp(:,idx)' ./ length(idx);
            iCtt = inv(fy_t(:,idx) * fy_t(:,idx)' ./ length(idx) + lamfy*eye(fno)); 
            Uttp{i,j} = iCtt * Cttp * A2'; 
            Uttp0{i,j} = iCtt * (fy_t(:,idx) * fy_tp(:,idx)' ./ length(idx)); 
            
            % Operator for passing message forward;             
            Ctpt = fy_tp(:,idx) * fy2_t(:,idx)' ./ length(idx); 
            iCtptp = inv(fy_tp(:,idx) * fy_tp(:,idx)' ./ length(idx) + lamfy * eye(fno)); 
            Utpt{i,j} = iCtptp * Ctpt * A2'; 
            Utpt0{i,j} = iCtptp * (fy_tp(:,idx) * fy_t(:,idx)' ./ length(idx));             

            Uttpij = Uttp{i,j}; 
            Uttp0ij = Uttp0{i,j}; 
            Utptij = Utpt{i,j};
            Utpt0ij = Utpt0{i,j}; 
            itsave([prefix, 'KBP', int2str(resono), '_fold', int2str(k), '_edge', int2str(i), '_', int2str(j), '.it'], ...
                Uttpij, Uttp0ij, Utptij, Utpt0ij);            
        end
    end        
       
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % testing; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
    % loop through each test sequence; 
    iter = 0; 
    for ti = test_idx
        angle = angleCell{ti}; 
        coord = angle2coord2(angle); 
        feat = featCell{ti}+eps; 
        atype = atypeCell{ti}; 
        chainlen = length(atype);
                
        iter = iter + 1; 
        itsave([prefix, 'KBP', int2str(resono), '_fold', int2str(k), '_test', int2str(iter), '.it'], ...
            angle, coord, feat, atype, chainlen); 
        
        msg_forward = cell(1, chainlen); 
        msg_backward = cell(1, chainlen); 
        belief = cell(1, chainlen); 
        likelihood = cell(1, chainlen); 
        
        % compute forward message; 
        for j = 1:chainlen-1                
            likelihood{j} = iCfyfy{atype(j)} * Cfyx{atype(j)} * iCxx{atype(j)} * feat(:,j);
            belief{j} = (ftesty' * likelihood{j}) .* (ftesty' * fmu{atype(j)});

            if (j < 2)
                msg_forward{j} = Utpt0{atype(j),atype(j+1)} * likelihood{j}; 
            else
                msg_forward{j} = Utpt{atype(j),atype(j+1)} * ((cfybasis'*msg_forward{j-1}) .* (cfybasis'*likelihood{j})); 
                belief{j} = belief{j} .* (ftesty' * msg_forward{j-1});
            end
        end

        likelihood{chainlen} = iCfyfy{atype(chainlen)} * Cfyx{atype(chainlen)} * iCxx{atype(chainlen)} * feat_t(:,j);
        belief{chainlen} = (ftesty'*likelihood{j}) .* (ftesty'*fmu{atype(j)}) .* (ftesty'*msg_forward{chainlen-1}); 

        % compute backward message; 
        for j = chainlen:-1:2
            if (j > chainlen -1)
                msg_backward{j} = Uttp0{atype(j-1),atype(j)} * likelihood{j}; 
            else
                msg_backward{j} = Uttp{atype(j-1),atype(j)} * ((cfybasis'*msg_backward{j+1}) .* (cfybasis'*likelihood{j})); 
                belief{j} = belief{j} .* (ftesty' * msg_backward{j+1});
            end
        end
        belief{1} = belief{1} .* (ftesty'*msg_backward{2}); 
        
        coord_pred = zeros(3, chainlen);
        for j = 1:chainlen
            [ignore, maxidx] = max(belief{j}); 
            coord_pred(:,j) = testy(:, maxidx);                         
        end

        err =  sum(coord_pred .* coord, 1);         
        errmat(curidx + (1:chainlen)) = err; 
        curidx = curidx + chainlen;
        
        fprintf(1, '--err %f and average err %f\n', mean(err), mean(errmat(1:chainlen)));        
        
%         tmp_pred = zeros(3, chainlen); 
%         for j = 1:chainlen
%             tmpscore = ftesty' * iCfyfy{atype(j)} * Cfyx{atype(j)} * iCxx{atype(j)} * feat_t(:,j);
%             [ignore, maxidx] = max(tmpscore); 
%             tmp_pred(:,j) = testy(:,maxidx); 
%         end
%         
%         err1 = sum(tmp_pred .* coord, 1); 
%         errmat1 = [errmat1, err1]; 
%         fprintf(1, '--err %f and average err %f, regression %f, %f\n', mean(err), mean(errmat), mean(err1), mean(errmat1));
    end    
end
end
