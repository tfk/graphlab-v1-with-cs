clear; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prepare denoising data to run in graphlab; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% addpath('/media/d/workspace/common/matlab');
% addpath('itpp-4.0.7/extras/');
addpath('uLSIF/'); 
% addpath('LSCDE/');

pno = 50; % 100; 

prefix = 'images/';
for resono = [1,2,3,12] %[1,2,3]
for src = 100 %[10, 25, 50, 75, 100, 125, 150, 175, 200, 225, 250]
    a = imread([prefix, 'source', int2str(src), '.pgm']);
    a = imresize(a, [pno, pno], 'nearest');
    [m, n] = size(a); 

    ua = unique(a);
    l = length(ua); 

    % observation noise standard deviation; 
    sigma = 30;
    
    randn('state', src); 
    rand('state', src);
    
    % flattened true image: 
    img1 = double(a(:)'); 
    % and flattened transposition of the true image; 
    aa = a';
    img2 = double(aa(:)'); 
    
    % noisy observation for training; 
    obs1 = double(a(:)') + sigma * randn(1, m*n);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Processing for original feature space of 
    % the true image; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    

    % find kernel bandwidth using median trick; 
    tmpidx = randperm(length(img1)); 
    tmpno = min([3000,length(img1)]); 
    dismat1 = pdist(img1(tmpidx(1:tmpno))').^2; 
    s1 = 0.5/median(dismat1);         
    % perform incomplete cholesky decomposition with Gaussian RBF kernel; 
    fno = 100; 
    expno = 1; 
    reso = 10.^(-resono);
    [fimg1, A1, I1] = incomplete_cholRBF(img1, s1, expno, fno, reso);
    fimg2 = incomplete_cholRBF_test(img2, img1(:,I1), s1, expno);
    
    % test points used in decoding the belief; 
    test = double(ua(:)');
    testno = length(test); 
    ftest = incomplete_cholRBF_test(test, img1(:,I1), s1, expno); 
    
    % parzen window as node potential 
    fmu = mean(fimg1, 2); 
    fno1 = length(I1);
    
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     % Processing for tensor product feature space
%     % of the true image; 
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
%     
%     expno = 4; 
%     [fimg5, A5, I5] = incomplete_cholRBF(img1, s1, expno, fno, reso); 
%     fno5 = length(I5); 
%     fimg6 = incomplete_cholRBF_test(img2, img1(:,I5), s1, expno);    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Processing for original feature space
    % of the noisy observations; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
    tmpidx = randperm(length(obs1)); 
    tmpno = min([3000,length(obs1)]); 
    dismat2 = pdist(obs1(tmpidx(1:tmpno))').^2;
    s2 = 1/median(dismat2);
    expno = 1; 
    [fobs1, A2, I2] = incomplete_cholRBF(obs1, s2, expno, fno, reso); 
    fno2 = length(I2);           
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % generate things related to 
    % embedding operators. 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    lfeat = [fimg1(:, 1:end-m), fimg1(:, m+1:end), fimg2(:, 1:end-n), fimg2(:, n+1:end)];
    rfeat = [fimg1(:, m+1:end), fimg1(:, 1:end-m), fimg2(:, n+1:end), fimg2(:, 1:end-n)];   

    % estimate conditional embedding operator for adjacent nodes; 
    lambda = 1e-2; %1e-2;    
%     pU = (A5 * lfeat * rfeat') / (rfeat * rfeat' + lambda * eye(fno1)); 
%     pU = pU';    
    pU = (rfeat * rfeat' + lambda * eye(fno1)) \ rfeat; 
    fimgbasis = lfeat;
    
    % estimate likelihood function;    
    lambda = 1e-2; %1e-2;
    pL = (fobs1 * fimg1') / (fimg1 * fimg1' + lambda * eye(fno1)); 
    pL = pL';

    lambda = 1e-6;
    alpha = (ftest' * ftest + eye(testno) * lambda) \ ones(length(ua), m*n);
    m1 = ftest * alpha; 
    m2 = m1; 
    m3 = m1; 
    m4 = m1; 
    belief = ones(l, m*n) ./ l; 

    isize = [m, n];
    m0 = m1(:,1); 

    tmp_prod_msg = fimgbasis' * pL; 
    tmp_ftest_pL = ftest' * pL; 
%     lfeat_m = zeros(length(I5),4); 
%     fimgbasis = fimg1(:,I5); 
   
    itsave([prefix, 'KBPlinear', int2str(resono), '_model', int2str(src), '_pno', int2str(pno), '.it'], pU, pL, fimgbasis, test, ftest, isize, m0, fmu, tmp_prod_msg, tmp_ftest_pL);  
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Generate test noisy observations and 
    % processing the feature matrix; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for ri = 1:10
        fprintf(1, '--randomization %d\n', ri);   

        obs2 = double(a(:)') + sigma * randn(1, m*n);    
        fobs2 = incomplete_cholRBF_test(obs2, obs1(:,I2), s2, expno);         
        
        itsave([prefix, 'Testlinear', int2str(resono), '_', int2str(src), '_', int2str(ri),  '_pno', int2str(pno), '.it'], obs2, fobs2, img1);
    end

%         tmp_prod_msg_fobs2 = pL * fobs2;
%         tmp_ftest_pL_fobs2 = tmp_ftest_pL * fobs2; 
%         
%         % perform loopy bp;    
%         errmat = []; 
%         ttmat = [];        
%     
%         iterno = 30;    
%         for iter = 1:iterno
%             tic; 
% 
%             for j = randperm(n)
%                 if (mod(j, 10) == 0)
%                     fprintf(1, '--iter %d, column %d \n', iter, j);            
%                 end
%                 for i = randperm(m)
% 
%                     ind = sub2ind([m, n], i, j);
%                     prod_msg = lfeat' * tmp_prod_msg_fobs2(:, ind);
%                     belief(:, ind) = tmp_ftest_pL_fobs2(:, ind);
% 
%                     % multiple incoming message from up; 
%                     if (i > 1)
%                         ind4 = sub2ind([m, n], i-1, j); 
%                         belief(:, ind) = belief(:, ind) .* (ftest' * m4(:, ind4)); 
%                         lfeat_m(:,4) = (lfeat' * m4(:,ind4));
%                         prod_msg = prod_msg .* lfeat_m(:,4);
%                     end
%                     % multiple incoming message from down; 
%                     if (i < m)
%                         ind2 = sub2ind([m, n], i+1, j); 
%                         belief(:, ind) = belief(:, ind) .* (ftest' * m2(:, ind2));
%                         lfeat_m(:,2) = (lfeat' * m2(:,ind2)); 
%                         prod_msg = prod_msg .* lfeat_m(:,2); 
%                     end
%                     % multiple incoming message from left; 
%                     if (j > 1)
%                         ind3 = sub2ind([m, n], i, j-1); 
%                         belief(:, ind) = belief(:, ind) .* (ftest' * m3(:, ind3));
%                         lfeat_m(:,3) = (lfeat' * m3(:,ind3)); 
%                         prod_msg = prod_msg .* lfeat_m(:,3); 
%                     end
%                     % multiple incoming message from right; 
%                     if (j < m)
%                         ind1 = sub2ind([m, n], i, j+1); 
%                         belief(:, ind) = belief(:, ind) .* (ftest' * m1(:, ind1));
%                         lfeat_m(:,1) = (lfeat' * m1(:,ind1)); 
%                         prod_msg = prod_msg .* lfeat_m(:,1); 
%                     end          
% 
%     %                 keyboard; 
%                     belief(:, ind) = belief(:, ind) .* (ftest' * fmu); 
% 
%                     lambda = 0.9; 
%                     % outgoing message to down; 
%                     if (i < m)
%                         m4(:, ind) = lambda * m4(:, ind) + ...
%                             (1-lambda) * (pU * (prod_msg ./ lfeat_m(:,2))); 
%                         m4(:, ind) = m4(:, ind) ./ sqrt(sum(m4(:, ind).^2));
%                     end
%                     % outgoing message to up; 
%                     if (i > 1)
%                         m2(:, ind) = lambda * m2(:,ind) + ... 
%                             (1-lambda) * (pU * (prod_msg ./ lfeat_m(:,4))); 
%                         m2(:, ind) = m2(:, ind) ./ sqrt(sum(m2(:, ind).^2));                    
%                     end
%                     % outgoing message to right; 
%                     if (j < m)
%                         m3(:, ind) = lambda * m3(:,ind) + ...
%                             (1-lambda) * (pU * (prod_msg ./ lfeat_m(:,1)));
%                         m3(:, ind) = m3(:, ind) ./ sqrt(sum(m3(:, ind).^2));
%                     end
%                     % outgoing message to left 
%                     if (j > 1)
%                         m1(:, ind) = lambda * m1(:,ind) + ...
%                             (1-lambda) * (pU * (prod_msg ./ lfeat_m(:,3))); 
%                         m1(:, ind) = m1(:, ind) ./ sqrt(sum(m1(:, ind).^2));
%                     end
% 
%     %                 keyboard; 
% 
%                 end
%             end
% 
%             tt = toc         
% 
%             % predict with current belief; 
%             [ignore, predx] = max(belief, [], 1); 
%             predimg = ua(predx); 
% 
%             err = sqrt(mean((img1' - double(predimg)).^2)); 
% 
%             errmat = [errmat, err]; 
%             ttmat = [ttmat, tt]; 
% 
%             fprintf(1, '-reso %d iter no %d current error is: %f\n', reso, iter, err); 
% 
%         end

end
end