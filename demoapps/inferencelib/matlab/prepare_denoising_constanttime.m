clear; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prepare denoising data to run in graphlab; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% addpath('/media/d/workspace/common/matlab');
% addpath('itpp-4.0.7/extras/');
addpath('uLSIF/'); 
% addpath('LSCDE/');

pno = 50; 

prefix = 'images/';
for resono = [1,2,3,12]
for src = 100 %[10, 25, 50, 75, 100, 125, 150, 175, 200, 225, 250]
    a = imread([prefix, 'source', int2str(src), '.pgm']);
    a = imresize(a, [pno, pno], 'nearest');
    [m, n] = size(a); 

    ua = unique(a)
    l = length(ua); 

    % observation noise standard deviation; 
    sigma = 30;
    
    randn('state', src); 
    rand('state', src);
    
    % flattened true image: 
    img1 = double(a(:)'); 
    % and flattened transposition of the true image; 
    aa = a';
    img2 = double(aa(:)'); 
    
    % noisy observation for training; 
    obs1 = double(a(:)') + sigma * randn(1, m*n);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Processing for original feature space of 
    % the true image; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    

    % find kernel bandwidth using median trick; 
    tmpidx = randperm(length(img1)); 
    tmpno = min([3000,length(img1)]); 
    dismat1 = pdist(img1(tmpidx(1:tmpno))').^2; 
    s1 = 0.5/median(dismat1);         
    % perform incomplete cholesky decomposition with Gaussian RBF kernel; 
    fno = 100; 
    expno = 1; 
    reso = 10.^(-resono);
    [fimg1, A1, I1] = incomplete_cholRBF(img1, s1, expno, fno, reso);
    fimg2 = incomplete_cholRBF_test(img2, img1(:,I1), s1, expno);
    
    % test points used in decoding the belief; 
    test = double(ua(:)');
    testno = length(test); 
    ftest = incomplete_cholRBF_test(test, img1(:,I1), s1, expno); 
    
    % parzen window as node potential 
    fmu = mean(fimg1, 2); 
    fno1 = length(I1);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Processing for tensor product feature space
    % of the true image; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
    
    expno = 4; 
    [fimg5, A5, I5] = incomplete_cholRBF(img1, s1, expno, fno, reso); 
    fno5 = length(I5); 
    fimg6 = incomplete_cholRBF_test(img2, img1(:,I5), s1, expno);    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Processing for original feature space
    % of the noisy observations; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
    tmpidx = randperm(length(obs1)); 
    tmpno = min([3000,length(obs1)]); 
    dismat2 = pdist(obs1(tmpidx(1:tmpno))').^2;
    s2 = 1/median(dismat2);
    expno = 1; 
    [fobs1, A2, I2] = incomplete_cholRBF(obs1, s2, expno, fno, reso); 
    fno2 = length(I2);           
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % generate things related to 
    % embedding operators. 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    lfeat = [fimg5(:, 1:end-m), fimg5(:, m+1:end), fimg6(:, 1:end-n), fimg6(:, n+1:end)];
    rfeat = [fimg1(:, m+1:end), fimg1(:, 1:end-m), fimg2(:, n+1:end), fimg2(:, 1:end-n)];   

    % estimate conditional embedding operator for adjacent nodes; 
    lambda = 1e-2;    
    pU = (A5 * lfeat * rfeat') / (rfeat * rfeat' + lambda * eye(fno1)); 
    pU = pU';
    
    % estimate likelihood function;    
    lambda = 1e-2;
    pL = (fobs1 * fimg1') / (fimg1 * fimg1' + lambda * eye(fno1)); 
    pL = pL';

    lambda = 1e-6;
    alpha = (ftest' * ftest + eye(testno) * lambda) \ ones(length(ua), m*n);
    m1 = ftest * alpha; 
    m2 = m1; 
    m3 = m1; 
    m4 = m1; 
    belief = ones(l, m*n) ./ l; 

    isize = [m, n];
    m0 = m1(:,1); 

    tmp_prod_msg = fimg1(:,I5)' * pL; 
    tmp_ftest_pL = ftest' * pL; 
    lfeat_m = zeros(length(I5),4); 
    fimgbasis = fimg1(:,I5); 
    
    itsave([prefix, 'KBP', int2str(resono), '_model', int2str(src), '_pno', int2str(pno), '.it'], pU, pL, fimgbasis, test, ftest, isize, m0, fmu, tmp_prod_msg, tmp_ftest_pL);  
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Generate test noisy observations and 
    % processing the feature matrix; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for ri = 1:10
        fprintf(1, '--randomization %d\n', ri);   

        obs2 = double(a(:)') + sigma * randn(1, m*n);    
        fobs2 = incomplete_cholRBF_test(obs2, obs1(:,I2), s2, expno);         
        
        itsave([prefix, 'Test', int2str(resono), '_', int2str(src), '_', int2str(ri), '_pno', int2str(pno), '.it'], obs2, fobs2, img1);
    end
end
end
