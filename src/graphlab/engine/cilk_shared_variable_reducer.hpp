#include <cilk/reducer.h>
namespace graphlab{
template<typename Graph>
class cilk_shared_variable
{
  public:
    // A bunch of typedefs. Not all of these are needed.
    typedef iengine<Graph> iengine_base;
    typedef typename iengine_base::vertex_id_type vertex_id_type;
    typedef typename iengine_base::update_task_type update_task_type;
    typedef typename iengine_base::update_function_type update_function_type;
    typedef typename iengine_base::ischeduler_type ischeduler_type;
    typedef typename iengine_base::imonitor_type imonitor_type;
    typedef typename iengine_base::termination_function_type termination_function_type;
    typedef typename iengine_base::iscope_type iscope_type;
    
    typedef typename iengine_base::sync_function_type sync_function_type;
    typedef typename iengine_base::merge_function_type merge_function_type;

  private:
     // define shared variable related functions
     sync_function_type sync_fun;
     glshared_base::apply_function_type apply_fun;
     merge_function_type merge_fun;
     graphlab::any identity;
     graphlab::any value;
     glshared_base* sharedvariable; 
  
  public:
     bool is_identity;
     cilk_shared_variable() {
       is_identity = true;
     }
 
     cilk_shared_variable(sync_function_type* s,
         glshared_base::apply_function_type* a,
         merge_function_type* m,
         glshared_base* shared_v,
         graphlab::any i,
         bool is_i) {
       sync_fun = *s;
       apply_fun = *a;
       merge_fun = *m;
       sharedvariable = shared_v;
       identity = i;
       value = i;
       is_identity = is_i;
     }
     
     void merge(cilk_shared_variable<Graph>* other) {
       // assume both are not null.
       merge_fun(value, other->value);
     }
     
     // takes a scope and accumulator.
     // TODO(tfk): get rid of the mixed pointer/reference passing.
     void reduce(iscope_type* scope) {
       sync_fun(*scope, value); 
     }

     void apply() {
       sharedvariable->apply(apply_fun, value);
     }
     void set (cilk_shared_variable* v) {
       *this = *v;
     }
};

template<typename Graph>
class cilk_shared_variable_reducer
{
   struct Monoid: cilk::monoid_base<cilk_shared_variable <Graph> >
   {
     static void reduce (cilk_shared_variable<Graph> *left, cilk_shared_variable<Graph> *right) {

       if (left->is_identity && right->is_identity) {
         printf("tfk_print: Error, error in reducers!!! \n");
         // this may not actually be an error.
         return;
       }

       if (left->is_identity) {
         *left = *right;
       }

       if (!left->is_identity && !right->is_identity) {
         left->merge(right);
       }
     }
   };

private:
   cilk::reducer<Monoid> imp_;
   cilk_shared_variable<Graph> leftmost_;
public:
   cilk_shared_variable<Graph>* identity;    
   cilk_shared_variable_reducer<Graph>::cilk_shared_variable_reducer() 
       : imp_(), leftmost_(){ }
   void 
   cilk_shared_variable_reducer<Graph>::set_identity(cilk_shared_variable<Graph>* set) {
     leftmost_ = *set;
     identity = set;
   }

   cilk_shared_variable<Graph>& view() {
     return imp_.view();
   }
};
};
 
